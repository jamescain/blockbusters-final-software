import numpy as np
from controller import *
import struct

#initialising everything below
robot = Robot()
leftSpeed = 0
rightSpeed = 0

timestep = int(robot.getBasicTimeStep())

maxspeed = 4 #A base value for speed
previous_top_distance = 0
top_distance = 0 #Reading from the upper distance sensor
previous_bottom_distance = 0
bottom_distance = 0 #Reading from the lower distance sensor
previous_bearing = 0
bearing = 0
bearing_targeted = None
previously_left_off = None

#Distance sensors
ds = []
dsNames = ['ds_long_low', 'ds_long_high']
for i in range(2):
    ds.append(robot.getDevice(dsNames[i]))
    ds[i].enable(timestep)

#Wheels
wheels = []
wheelsNames = ['wheel1', 'wheel2']
for i in range(2):
    wheels.append(robot.getDevice(wheelsNames[i]))
    wheels[i].setPosition(float('inf'))
    wheels[i].setVelocity(0.0)

distanceDiff = 0 #Difference between distance sensor readings
previousDiff = 0

#Detection counter
detections = -1 #-1 because a count is achieved during settling

#GPS
gps = []
gpsNames = ['gps_body']
for i in range(1):
    gps.append(robot.getDevice(gpsNames[i]))
    gps[i].enable(timestep)

#Compass
compass = []
compassNames = ['compass_body']
for i in range(1):
    compass.append(robot.getDevice(compassNames[i]))
    compass[i].enable(timestep)

#Light sensors
red_light_sensor = robot.getDevice('red_light_sensor')
green_light_sensor = robot.getDevice('green_light_sensor')
green_light_sensor.enable(timestep)
red_light_sensor.enable(timestep)

#Pincers
rightpincer = robot.getDevice('rightpincer')
leftpincer = robot.getDevice('leftpincer')
leftpincer.setPosition(0)
rightpincer.setPosition(0)

#Position sensors
ps = []
psNames = ['leftpincer_ps', 'rightpincer_ps']
for i in range(2):
    ps.append(robot.getDevice(psNames[i]))
    ps[i].enable(timestep)

#Emitter
emitter = []
emitterNames = ['emitter']
for i in range(1):
    emitter.append(robot.getDevice(emitterNames[i]))

#Receiver
receiver = []
receiverNames = ['receiver']
for i in range(1):
    receiver.append(robot.getDevice(receiverNames[i]))
    receiver[i].enable(timestep)

#Multipliers for setting speeds
speed_multiplier_a = 0.3
speed_multiplier_b = 2
speed_multiplier_c = 0.15

#More initialisation below
f_bearing = None #Bearing of far side of box
i_bearing = None #Bearing of near side of box

red_locations = []
green_locations = []

approaching = False #Specifies that a robot is approaching a box

colourresult = None #Specifies what colour a robot is detecting
colourset = None #Sets a colour
colourrobot = None #The colour of the robot
othercolour = None #The colour that is not of the robot

def set_robot_colour(initial_gps):
    """Setting robot colour"""
    if initial_gps[2] < 0:
        robot_colour = 'Green'
        other_colour = 'Red'
    elif initial_gps[2] > 0:
        robot_colour = 'Red'
        other_colour = 'Green'
    return robot_colour, other_colour

#More initialisation below
localist = [] #List of previous positions that is used to determine if a robot is stuck
for i in range(31):
    localist.append([2,2,2])
localist[-1] = [4,4,4]

searching = False #Specifies whether a robot is searching for a box or investigating a box
box_in_hold = False #Specifies that the robot has captured a box and is returning it
box_dropping = False #Specifies that a robot is dropping off a box
box_dropped = False #Specifies that a robot has dropped a box and is retreating
goNext = True #Dictates the robot to continue with a task
what_to_do = 0 #Specifies the current search area/method of the robot
next_mission = False #Dictates the robot to continue to a next mission
half_measure = False #Dictates that the robot shall only advance by half a mission

rotEnd = None #Specifies where a given rotationary search is to end
first_pass = True

stuck = False #Specifies whether a robot is stuck or not
previousStuck = False
stuck_at = [0,0,0] #Specifies where the robot is stuck

gps_bod = [0,0,0]
gps_bod_arr = np.array(gps_bod)
gps_red = [0,0,0.4] #Specifies location of red square
gps_green = [0,0,-0.4] #Specifies location of green square
box_found_at = [0,0,0]

#Below are specified parameters for different searches
m0,s0,s1,s2,s3,s4,s5,s6,m0s0 = None,None,None,None,None,None,None,None,None
m0sta,m0end,r0sta,r0end,r1sta,r1end,r2sta,r2end,r3sta,r3end = None,None,None,None,None,None,None,None,None,None
r4sta,r4end,r5sta,r5end,r6sta,r6end= None,None,None,None,None,None

other_robot_pos = np.array([[0,0,0]])
other_robot_box_pos = np.array([[0,0,0]])
my_box_pos = np.array([[0,0,0]])
other_robot_search_complete = False
first_middle = False
all_middle = False
close_range = False
reverse = False #Whether a robot is reversing

def searchPath(colourrobot):
    """Specifing search points and start/end rotations"""
    if colourrobot == 'Red':
        
        m0 = [0,0,0.4]
        s0 = [0,0,-0.42]
        
        s1 = [0.2,0,0]
        s2 = [0.35,0,0.6]
        s3 = [-0.3,0,0.75]
        s4 = [-0.2,0,0]
        s5 = [-0.35,0,-0.6]
        s6 = [0.3,0,-0.75]

        m0s0 = [0.3,0,0] 
        m0end = 135
        m0sta = 225
        
        r0end = 35
        r0sta = 325

        r1end = 250
        r1sta = 0
        
        r2end = 170
        r2sta = 90
        
        r3end = 260
        r3sta = 190
        
        r4end = 70
        r4sta = 180
        
        r5end = 350
        r5sta = 270
        
        r6end = 80
        r6sta = 10

    elif colourrobot == 'Green':
        
        s0 = [0,0,0.42]
        m0 = [0,0,-0.4]
        
        s4 = [0.2,0,0]
        s5 = [0.35,0,0.6]
        s6 = [-0.3,0,0.75]
        s1 = [-0.2,0,0]
        s2 = [-0.35,0,-0.6]
        s3 = [0.3,0,-0.75]

        m0s0 = [-0.3,0,0] 
        m0end = 315
        m0sta = 45
        
        r0end = 215
        r0sta = 135

        r4end = 250 
        r4sta = 0 
        
        r5end = 170
        r5sta = 90
        
        r6end = 260
        r6sta = 190
        
        r1end = 70
        r1sta = 180
        
        r2end = 350
        r2sta = 270
        
        r3end = 80
        r3sta = 10
    return m0,s0,s1,s2,s3,s4,s5,s6,m0s0,m0sta,m0end,r0sta,r0end,r1sta,r1end,r2sta,r2end,r3sta,r3end,r4sta,r4end,r5sta,r5end,r6sta,r6end

transition = True #Whether a robot is to transition to a point to get to another desired point
midTrans = True #If a robot is currently transitioning
close_complete = False #If the robot has completed its close range search
wtd4_is_done = 0 #If search at what_to_do = 4 is complete for the other robot this is incremented
wtd4_is_done_here = 0 #If search at what_to_do = 4 is complete for this robot this is incremented
wtd8_is_done = 0 #If search at what_to_do = 7 is complete for the other robot this is incremented
wtd8_is_done_here = 0 #If search at what_to_do = 7 is complete for this robot this is incremented
other_robot_done = False

dropping_from_here = None #Location of where the box is being dropped from

boxes_delivered = 0 #Counter for how many boxes have been delivered

def rotate(x):
    """Rotate at x multiplied by maxspeed(defined above)"""
    wheels[0].setVelocity(x * maxspeed)
    wheels[1].setVelocity(-x * maxspeed)
    
def stop():
    """Stop moving"""
    wheels[0].setVelocity(0)
    wheels[1].setVelocity(0)
    
def move(x):
    """Move at x multiplied by maxspeed(defined above)"""
    wheels[0].setVelocity(x * maxspeed)
    wheels[1].setVelocity(x * maxspeed)

def getDistDiff(ds):
    """Difference in readings from the higher and lower distance sensors"""
    return abs(ds[0].getValue() - ds[1].getValue())


def get_ls_colour(green_value, red_value):
    """Colour determination from the light sensors"""
    if green_value >= 89:
        colour = 'Green'
    elif red_value >= 89:
        colour = 'Red'
    else:
        colour = None
    return colour

def get_bearing() :
    """Calculating bearings from raw values from the compass"""
    values = compass[0].getValues()
    rad = np.arctan2(values[0], values[2])
    bearing = (rad - (np.pi/2))/np.pi*180
    if bearing < 0:
        bearing = bearing + 360
    return bearing

def roboSpin(robo,need):
    """Spinning the robot towards the desired direction, the fastest way depending on the orientation of the robot"""
    if robo < 180:
        if need < robo:
            #anticloskwise
            leftSpeed = -0.5
            rightSpeed = 0.5
        else:
            if (need-robo) < 180:
                #clockwise
                leftSpeed = 0.5
                rightSpeed = -0.5
            else:
                #antilockwise
                leftSpeed = -0.5
                rightSpeed = 0.5
    else:
        if need > robo:
            #clockwise
            leftSpeed = 0.5
            rightSpeed = -0.5
        else:
            if (robo-need) < 180:
                #anticlockwise
                leftSpeed = -0.5
                rightSpeed = 0.5
            else:
                #clockwise
                leftSpeed = 0.5
                rightSpeed = -0.5
    return leftSpeed, rightSpeed

def goToGPS(destination, rotation=-1):
    """takes a GPS array and makes the robot drive to it"""
    gps_dest = np.array(destination)
    gps_bod = np.array(gps[0].getValues())
    gps_diff = gps_dest - gps_bod
    e = 0.02 #error allowed
    arrived = False #Turns True when robot has arrived to its destination
    distance = ((gps_bod[0]-gps_dest[0])**2 + (gps_bod[2]-gps_dest[2])**2)**0.5
    rad_needed = np.arctan2(gps_diff[0], gps_diff[2])    
    bearing_needed = (rad_needed)/np.pi*180
    if bearing_needed < 0:
        bearing_needed = -bearing_needed
    else:
        bearing_needed = 360 - bearing_needed
    robo_bearing = get_bearing()
    bearing_err = 0.6/distance
    re = 3
    if distance < e:
        if rotation == -1:
            leftSpeed = 0
            rightSpeed = 0
            arrived = True
        else:
            if rotation-re < robo_bearing < rotation+re:
                leftSpeed = 0
                rightSpeed = 0
                arrived = True
            else:
                leftSpeed,rightSpeed = roboSpin(robo_bearing,rotation)
    else:
        be = bearing_err
        if (bearing_needed-be) < robo_bearing < (bearing_needed+be):
            leftSpeed = 1
            rightSpeed = 1
        else:
            leftSpeed,rightSpeed = roboSpin(robo_bearing,bearing_needed)
     
    wheels[0].setVelocity(leftSpeed * maxspeed)
    wheels[1].setVelocity(rightSpeed * maxspeed)
    
    if arrived == True:
        return 1

def open_pincers():
    """Opens the pincers of the robot"""
    leftpincer.setPosition(np.pi / 6)
    rightpincer.setPosition(-np.pi / 6)
    
def close_pincers():
    """Closes the pincers of the robot"""
    leftpincer.setPosition(0)
    rightpincer.setPosition(0)


def are_pincers_closed():
    """Checking whether the pincers are closed"""
    if abs(ps[0].getValue()) < 0.2 and abs(ps[1].getValue()) < 0.2:
        return True
    else:
        return False


def check_box():
    """Checking whether the box being approached towards is still there"""
    gvalue = green_light_sensor.getValue()
    rvalue = red_light_sensor.getValue()
    if are_pincers_closed() == False:
        if get_ls_colour(gvalue, rvalue) == None:
            close_pincers()
            stop()
        else:
            close_pincers()
    else:
        pass
    

def bearingneed(gps_dest, gps_bod):
    """Calculates the bearing that the robot needs to be oriented towards in order to travel towards a required destination"""
    gps_dest_arr = np.array(gps_dest)
    gps_bod_arr = np.array(gps_bod)
    gps_diff = gps_dest_arr - gps_bod_arr
    rad_needed = np.arctan2(gps_diff[0], gps_diff[2])    
    bearing_needed = (rad_needed)/np.pi*180
    if bearing_needed < 0:
        bearing_needed = -bearing_needed
    else:
        bearing_needed = 360 - bearing_needed
    return bearing_needed

def receive():
    """Robots receive communciation depending on what was sent"""
    typ = None
    position_data = None 
    if receiver[0].getQueueLength() >0:
            message = receiver[0].getData()
            dataList = struct.unpack('hddd', message)
            receiver[0].nextPacket()
            position_data = np.array([[dataList[1], dataList[2], dataList[3]]])
            #print(dataList)
            if dataList[0] == 1:
                typ = "Robot"
                global other_robot_pos
                other_robot_pos = np.append(other_robot_pos, position_data, axis = 0) 
            elif dataList[0] == 2:
                typ = "Box"
                global my_box_pos
                my_box_pos= np.append(my_box_pos, position_data, axis = 0)
            elif dataList[0] == 3:
                typ = "Search_finish"
                global other_robot_search_complete
                other_robot_search_complete = True
            elif dataList[0] == 4:
                typ = "First_middle_done"
                global first_middle
                first_middle = True
            elif dataList[0] == 5:
                typ = "All_middle_done"
                global all_middle
                all_middle = True
            elif dataList[0] == 6:
                typ = "Close_range_done"
                global close_range
                close_range = True
            elif dataList[0] == 7:
                typ = "wtd4_done"
                global wtd4_is_done
                wtd4_is_done += 1
            elif dataList[0] == 8:
                typ = "wtd8_done"
                global wtd8_is_done
                wtd8_is_done += 1
            elif dataList[0] == 9:
                typ = "robot_done"
                global other_robot_done
                other_robot_done = True
            else:
                pass
    else:
        pass
    
def send(typ=None, position_data = None):
    """Robots send information depending on inputs of function"""
    if typ == "Robot":
        message = struct.pack('hddd', 1, position_data[0], position_data[1], position_data[2]) #Sends location of robot
        emitter[0].send(message)
    elif typ == "Box":
        message = struct.pack('hddd', 2, position_data[0], position_data[1], position_data[2]) #Sends location of box
        emitter[0].send(message)
    elif typ == "Search_finish":
        message = struct.pack('hddd', 3, 3, 3, 3) #Sends message that a search has been completed
        emitter[0].send(message)
    elif typ == "First_middle_done":
        message = struct.pack('hddd', 4, 4, 4, 4) #Sends message that the middle search of the red robot is complete
        emitter[0].send(message)
    elif typ == "All_middle_done":
        message = struct.pack('hddd', 5, 5, 5, 5) #Sends message that the middle search of the green robot is complete
        emitter[0].send(message)
    elif typ == "Close_range_done":
        message = struct.pack('hddd', 6, 6, 6, 6) #Sends message that the close range search is complete
        emitter[0].send(message)
    elif typ == "wtd4_done":
        message = struct.pack('hddd', 7, 7, 7, 7) #Sends message that a robot has completed search at what_to_do = 4
        emitter[0].send(message)
    elif typ == "wtd8_done":
        message = struct.pack('hddd', 8, 8, 8, 8) #Sends message that a robot has completed search at what_to_do = 7
        emitter[0].send(message)
    elif typ == "robot_done":
        message = struct.pack('hddd', 9, 9, 9, 9) #Sends message that a robot has completed all its tasks
        emitter[0].send(message)
    else:
        pass

def box_bearing(i_bearing,f_bearing):
    """Further specifies the bearing towards a box that is to be investigated"""
    if 0 < i_bearing < 90:
        if 270 < f_bearing < 360:
            if i_bearing > (360-f_bearing):
                box_bear1 = (i_bearing - (360 - f_bearing))/2
            elif i_bearing < (360-f_bearing):
                box_bear1 = (360 + f_bearing + i_bearing)/2
        elif 0 < f_bearing < 90:
            box_bear1 = (i_bearing + f_bearing)/2
        else:
            box_bear1 = (i_bearing + f_bearing)/2
    else:
        box_bear1 = (i_bearing + f_bearing)/2
    return box_bear1

def prev_left_off(box_bear1):
    """Records what bearing a radial search was interrupted at, in order to return to it afterwards"""
    if 340 < box_bear1 < 360:
        previously_left_off = (box_bear1 + 20) - 360
    else:
        previously_left_off = box_bear1 + 20 #If box is of the wrong colour, adding 20 degrees to the bearing avoids scanning it again
    return previously_left_off

def stationary_search_response(f_bearing, i_bearing, bearing, rotEnd, what_to_do, previous_bottom_distance, bottom_distance, colourrobot, half_measure,close_complete, wtd4_is_done, wtd8_is_done, wtd4_is_done_here, wtd8_is_done_here):
    """Function defines how the robot should behave while conducting a stationary radial search"""
    distance_to_box = 0
    if f_bearing != None and i_bearing != None and what_to_do != 1 and what_to_do != 0: #Behaviour of search after the two initial search locations
        goNext = False
        next_mission = False
        searching = True
        half_measure = False
        box_bear = box_bearing(i_bearing,f_bearing)
        previously_left_off = prev_left_off(box_bear)
        if bearing > (-1 + box_bear) and bearing < (1 + box_bear): #If bearing is correct, robot approaches
            distance_to_box = ds[0].getValue()
            approach = True
        else: #Otherwise it continues spinning to adjust its bearing
            rotate(speed_multiplier_c)
            approach = False
    elif f_bearing != None and i_bearing != None and (what_to_do == 1 or what_to_do ==0) and (previous_bottom_distance < 900 or bottom_distance < 900): #Behaviour of search during the two initial search locations, limited in range
        goNext = False
        next_mission = False
        searching = True
        half_measure = False
        box_bear = box_bearing(i_bearing,f_bearing)
        previously_left_off = prev_left_off(box_bear)
        if bearing > (-1 + box_bear) and bearing < (1 + box_bear):
            approach = True
        else:
            rotate(speed_multiplier_c)
            approach = False
    else: #Robot is rotating in order to detect something
        rotate(-speed_multiplier_c)
        approach = False
        previously_left_off = None
        if bearing > rotEnd - 1 and bearing < rotEnd + 1: #If the bearing reached the previously specified end bearing of this search, the robot goes to its next mission
            goNext = True
            next_mission = True
            searching = False
            half_measure = False
            #On certain occasions below a message is sent to the other robot about the completion of this search
            if what_to_do == 0 and colourrobot == 'Red':
                send('First_middle_done')
                next_mission = False
                half_measure = True
            if what_to_do == 0 and colourrobot == 'Green':
                send('All_middle_done')
                what_to_do = 0.8
                next_mission = False
            if what_to_do == 1:
                send('Close_range_done')
                close_complete = True
                next_mission = False
                half_measure = True
            if what_to_do == 4:
                send('wtd4_done')
                wtd4_is_done_here += 1
                next_mission = False
                half_measure = True
            if what_to_do == 7:
                send('wtd8_done')
                wtd8_is_done_here += 1
                next_mission = False
                half_measure = True
        else:
            goNext = False
            next_mission = False
            searching = True
            half_measure = False
    return approach, goNext, next_mission, searching, previously_left_off, half_measure, close_complete, wtd4_is_done, wtd8_is_done, wtd4_is_done_here, wtd8_is_done_here, distance_to_box, what_to_do

def approaching_box_response(distanceDiff,previousDiff,goNext, searching, i_bearing, f_bearing, approaching, previously_left_off):
    """Dictates how a robot is approaching a box"""
    diff = distanceDiff
    box_dist = ds[0].getValue()
    if (diff < 50) and (box_dist > 300): 
        goNext = True
        searching = False
        i_bearing = None
        f_bearing = None
        approaching = False
    else: #Approaches a box if the difference in readings is significant
        move(speed_multiplier_b)
        if box_dist < 300:
            open_pincers()
    return goNext, searching, i_bearing, f_bearing, approaching,previously_left_off

def immediate_colour_detection_response(colourresult, previously_left_off, colourrobot):
    """Dictates the initial behaviour of robot to a new colour detection"""
    colourset = colourresult #Setting colour to the one detected
    print(colourrobot + " robot detected " + colourresult + " box") #Prints detection of colour to terminal
    i_bearing = None
    f_bearing = None
    approaching = False
    box_found_at = gps[0].getValues()
    if colourset != colourrobot:
        if 0 < previously_left_off < 22:
            previously_left_off = (previously_left_off - 22) + 360
        else:
            previously_left_off = previously_left_off - 22
        if othercolour == 'Green': #Appends location of box to be investigated later if colour doesn't match
            green_locations.append(box_found_at)
        if othercolour == 'Red':
            red_locations.append(box_found_at)
    return colourset, i_bearing, f_bearing, box_found_at, approaching, previously_left_off

def followup_colour_detection_response(colourset, colourrobot, othercolour, gps_bod, box_found_at, distance_to_box):
    """Dictates what the robot is to do after detecting the colour of a box"""
    distance_travelled = ((gps_bod[0] - box_found_at[0])**2 + (gps_bod[2] - box_found_at[2])**2)**0.5
    if colourset == colourrobot: #Response if the colour is correct
        close_pincers() #Enclose box
        if are_pincers_closed() == False: #Check if pincers are closed
            box_in_hold = False
            searching = True      
            goNext = False
            reverse = False  
            stop()
        else:
            if distance_travelled < distance_to_box/2500: #Back away slightly if box is in hold
                check_box()
                move(-speed_multiplier_b)
                box_in_hold = False
                searching = True      
                goNext = False
                reverse = True 
            else:
                box_in_hold = True
                searching = False      
                goNext = False
                reverse = False
                close_pincers()    
    elif colourset == othercolour: #Back out if the colour doesn't match
        box_in_hold = False
        if distance_travelled < 0.2:
            move(-speed_multiplier_b)
            searching = True
            goNext = False
            reverse = True
        else:
            stop()
            close_pincers()
            searching = False
            goNext = True
            reverse = False
    return box_in_hold, searching, goNext, reverse 

def in_wake(colourrobot):
    """Function for checking if the robot is in the "wake" of the other robot's square. 
    The robot then goes to a coordinate past the box, for the robot to then to carry on to its own square
    To be used in other functions such as delivery function"""
    avoided = 0
    if colourrobot == "Green": #Avoidance sequence for the green robot
        gps_bod = np.array(gps[0].getValues())
        gps_pos = gps_bod - gps_green
        theta = np.arctan(gps_pos[0]/gps_pos[2])
        if gps_bod[2] > 0.6: 
            if  abs(theta) < np.arctan(0.2/0.4):
                if gps_bod[0] > 0:
                    avoided = goToGPS([0.8, 0, 0.7])
                elif gps_bod[0] < 0:
                    avoided = goToGPS([0.8, 0, 0.7])      
            else:
                avoided = 1
        else:
            avoided = 1
            
    if colourrobot == "Red": #Avoidance sequence for the red robot
        gps_bod = np.array(gps[0].getValues())
        gps_pos = gps_bod - gps_red
        theta = np.arctan(gps_pos[0]/gps_pos[2])
    
        if gps_bod[2] < -0.6: 
            if  abs(theta) < np.arctan(0.2/0.4):
                if gps_bod[0] > 0:
                    avoided = goToGPS([-0.8, 0, -0.7])
                elif gps_bod[0] < 0:
                    avoided = goToGPS([-0.8, 0, -0.7])      
            else:
                avoided = 1
        else:
            avoided = 1

    return avoided

def delivering_response(colourset, bearing, gps_bod, gps_red, gps_green, what_to_do, colourrobot,midTrans,box_dropping,dropping_from_here, previously_left_off, box_found_at):
    """Dictates how the robot is to deliver a box that has been picked up"""
    close_pincers()
    if what_to_do == 0: #Where to go if what_to_do = 0
        if colourrobot == 'Red':
            done = goToGPS([-0.45,0,0])
        elif colourrobot == 'Green':
            done = goToGPS([0.45,0,0])
        gps_set = None
        if done == 1:
            stop()
            open_pincers()
            box_dropping = True
            dropping_from_here = gps[0].getValues()
        else:
            box_dropping = False
            dropping_from_here = None
    elif what_to_do == 1: #Where to go if what_to_do = 1
        if colourrobot == 'Red':
            inter = [0.15,0,-0.2]
            drop_off = [0.15,0,0.05]
        elif colourrobot == 'Green':
            inter = [-0.15,0,0.2]
            drop_off = [-0.15,0,-0.05]
        if midTrans == True:
            done_int = goToGPS(inter)
            if done_int == 1:
                midTrans = False
        else:
            done = goToGPS(drop_off)
            if done == 1:
                stop()
                open_pincers()
                box_dropping = True
                dropping_from_here = gps[0].getValues()
                midTrans = True
            else:
                box_dropping = False
                dropping_from_here = None
        gps_set = None
    else: #Where to go if what_to_do is anything other than 0 or 1
        if colourset == 'Red':
            targeting = bearingneed(gps_red, gps_bod)
            gps_set = gps_red
        elif colourset == 'Green':
            targeting = bearingneed(gps_green, gps_bod)
            gps_set = gps_green
        dodged = in_wake(colourrobot)
        if dodged != 1:
            what_to_do = 6
            previously_left_off = None
        if dodged == 1:

            return_difference = ((gps_set[0] - gps_bod[0])**2 + (gps_set[2] - gps_bod[2])**2)**0.5
            
            if (((bearing - 10) < targeting < (bearing + 10)) and (return_difference < 0.35)):    #If robot is very close to its drop off area and facing the correct orientation
                stop()
                open_pincers()
                box_dropping = True
                dropping_from_here = gps[0].getValues()
            else:
                goToGPS(gps_set)
                box_dropping = False
                dropping_from_here = None
    
    return box_dropping, dropping_from_here, gps_set, midTrans, what_to_do, previously_left_off
    
def dropping_box_response(gps_bod, dropping_from,what_to_do,boxes_delivered):
    """Dictates the behaviour of the robot as it is dropping some of its boxes"""
    if what_to_do != 1 and what_to_do != 0:
        distance_travelled = ((gps_bod[0] - dropping_from[0])**2 + (gps_bod[2] - dropping_from[2])**2)**0.5
        if distance_travelled < 0.15: #push box to centre
            move(speed_multiplier_b)
            box_dropped = False
            dropped_at_here = None
        else:
            stop()
            box_dropped = True #At a distance box is being dropped
            dropped_at_here = gps[0].getValues() 
            if what_to_do != 0:
                boxes_delivered += 1 #In what_to_do = 0 boxes aren't actually delivered to their zones
            if boxes_delivered > 3: #If all 4 boxes have been delivered
                box_dropped = False
                if distance_travelled < 0.3:
                    move(speed_multiplier_b) #Approach the final destination 
                else:
                    stop()
                    exit() #Robot has completed all of its objectives completely
        box_in_hold = False
        if boxes_delivered == 4:
            box_in_hold = True
    elif what_to_do == 1:
        distance_travelled = ((gps_bod[0] - dropping_from[0])**2 + (gps_bod[2] - dropping_from[2])**2)**0.5
        if distance_travelled < 0.2:
            move(-speed_multiplier_b)
            box_dropped = False
            dropped_at_here = None
        else:
            stop()
            box_dropped = True
            dropped_at_here = gps[0].getValues() 
            if what_to_do != 0:    
                boxes_delivered += 1
        box_in_hold = False
    elif what_to_do == 0:
        box_dropped = True
        dropped_at_here = gps[0].getValues()
        box_in_hold = False
    return box_dropped, box_in_hold, dropped_at_here,boxes_delivered

def after_dropping_box_response(gps_bod, dropped_at,what_to_do):
    """Dictates what the robot will do after it has dropped off a box"""
    if what_to_do != 1: #Behaviour if what_to_do is not 1
        box_dropping = False
        colourset = None
        distance_travelled = ((gps_bod[0] - dropped_at[0])**2 + (gps_bod[2] - dropped_at[2])**2)**0.5
        if distance_travelled < 0.25: #Backing away from the area where the box has been dropped
            move(-speed_multiplier_b)
            goNext = False
        else:
            stop()
            close_pincers()
            goNext = True
    elif what_to_do == 1: #Behaviour if what_to_do = 1
        box_dropping = False
        colourset = None
        distance_travelled = ((gps_bod[0] - dropped_at[0])**2 + (gps_bod[2] - dropped_at[2])**2)**0.5
        close_pincers()
        if distance_travelled < 0.2: #Pushing the box into the ideal place for it to rest in the corner of the square
            move(speed_multiplier_b)
            goNext = False
        else:
            stop()
            goNext = True
    return box_dropping, colourset, goNext

def go_to_next_search_response(what_to_do, next_mission, half_measure, previously_left_off,transition, colourrobot,close_complete, wtd4_is_done_here, wtd8_is_done_here, time, m0,s0,s1,s2,s3,s4,s5,s6,m0s0,m0sta,m0end,r0sta,r0end,r1sta,r1end,r2sta,r2end,r3sta,r3end,r4sta,r4end,r5sta,r5end,r6sta,r6end):
    """Dictates what the robot is to do
    First middle search, then short distance search from other side
    Afterwards robot is sent to several search areas around the arena"""
    if next_mission == True: #Sending robot to its next mission
        what_to_do+=1
        next_mission = False
    if half_measure == True: #Sending robot to an intermediate mission
        what_to_do = what_to_do + 0.5
        half_measure = False
    if what_to_do == 8: #Robot cycles through missions
        what_to_do = 2
    searching = False
    box_dropped = False
    goNext = True
    colourset = None
    rotEnd = None
    if what_to_do == 0: #Specifies behaviour of robot in what_to_do = 0, being the middle search
        if colourrobot == 'Red':
            if previously_left_off != None:
                bearing_targeted = previously_left_off
            else:
                bearing_targeted = 225
            done = goToGPS([0,0,0.4], bearing_targeted)
            rotEnd = 135
            if done == 1:
                searching = True
                goNext = False
        elif colourrobot == 'Green':
            stop()
            receive()
            if first_middle == True:
                if previously_left_off != None:
                    bearing_targeted = previously_left_off
                else:
                    bearing_targeted = 45
                done = goToGPS([0,0,-0.4], bearing_targeted)
                rotEnd = 315
                if done == 1:
                    searching = True
                    goNext = False
    if what_to_do == 0.5: #Intermediate mission of red robot waiting for green to complete its middle search
        receive()
        if all_middle == True:
            what_to_do = 0.8
        else:
            done = goToGPS([0,0,0.4], 0)
            if done == 1:
                stop()

    if what_to_do == 0.8: #Intermediate mission for green robot to be sent to what_to_do = 1
        done = goToGPS(m0s0)
        if done == 1:
            what_to_do = 1

    if what_to_do == 1.5: #Intermediate mission for the robots to wait for the other one complete its close range search
        if colourrobot == 'Red':
            receive()
            if close_range == True:
                what_to_do = 1.8
            else:
                done = goToGPS([0,0,-0.4], 180)
               
        elif colourrobot == 'Green':
            receive()
            if close_range == True:
                what_to_do = 1.8
            else:
                done = goToGPS([0,0,0.4], 0)
    
    if what_to_do == 1.8:
        send('Close_range_done')
        what_to_do = 2

    if what_to_do == 4.5: #Intermediate mission as robot waits for the other one to complete its own mission what_to_do = 4, in order to have robots seperated in the two halves of the arena
        if time > 180:
            what_to_do = 5
        else:
            if colourrobot == 'Red':
                receive()
                if wtd4_is_done == wtd4_is_done_here:
                    what_to_do = 5  #When both robots have finished they go to the next search
                else:
                    done = goToGPS(s3, 180)
            elif colourrobot == 'Green':
                receive()
                if wtd4_is_done == wtd4_is_done_here:
                    what_to_do = 5 
                else:
                    done = goToGPS(s3, 0)

    if what_to_do == 7.5: #Intermediate mission as robot waits for the other one to complete its own mission what_to_do = 7, in order to have robots seperated in the two halves of the arena
        if time > 180:
            what_to_do = 2
        else:
            if colourrobot == 'Red':
                receive()
                if wtd8_is_done == wtd8_is_done_here:
                    what_to_do = 2  #When both robots have finished they go to the next search
                else:
                    done = goToGPS(s6, 0)
            elif colourrobot == 'Green':
                receive()
                if wtd8_is_done == wtd8_is_done_here:
                    what_to_do = 2 
                else:
                    done = goToGPS(s6, 180)
            
            
    if what_to_do != 0 and what_to_do != 0.5 and what_to_do != 1.5 and what_to_do != 4.5 and what_to_do != 7.5 and what_to_do != 0.8: #Response for all missions apart from the predefined ones
        if what_to_do == 1: #Close range search from the other robots square
            search_point = s0
            search_begin = r0sta
            search_end = r0end
            intermediate = None
        elif what_to_do == 2: #This and the following states define several radial search locations around the arena
            search_point = s1
            search_begin = r1sta
            search_end = r1end
            intermediate = None
        elif what_to_do == 3:
            search_point = s2
            search_begin = r2sta
            search_end = r2end
            intermediate = None
        elif what_to_do == 4:
            search_point = s3
            search_begin = r3sta
            search_end = r3end
            intermediate = None
        elif what_to_do == 5:
            search_point = s4
            search_begin = r4sta
            search_end = r4end
            intermediate = None
        elif what_to_do == 6:
            search_point = s5
            search_begin = r5sta
            search_end = r5end
            intermediate = None
        elif what_to_do == 7:
            search_point = s6
            search_begin = r6sta
            search_end = r6end
            intermediate = None

        if previously_left_off != None: #Continue search from the bearing that it was previously left off at
            bearing_targeted = previously_left_off
        else: #Otherwise from where search is meant to begin
            bearing_targeted = search_begin
        if intermediate != None: #If there is an intermediate step to going to this search area, robot shall go through there first
            if transition == True:
                done_int = goToGPS(intermediate)
                if done_int == 1:
                    transition = False
            else:
                done = goToGPS(search_point, bearing_targeted)
                rotEnd = search_end
                if done == 1:
                    searching = True
                    goNext = False
                    transition = True
        else:
            done = goToGPS(search_point, bearing_targeted)
            rotEnd = search_end
            if done == 1:
                searching = True
                goNext = False
    return searching, box_dropped, goNext, colourset, what_to_do, next_mission, rotEnd, transition, half_measure, close_complete
