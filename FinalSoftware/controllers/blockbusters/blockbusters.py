"""exp controller."""
import time

import numpy as np
from controller import *

from setupAndFunctions import *

# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:
    time = robot.getTime()

    #Distance sensor readings, and associated differences
    previous_top_distance = top_distance
    top_distance = ds[1].getValue()
    previous_bottom_distance = bottom_distance
    bottom_distance = ds[0].getValue()
    previousDiff = distanceDiff
    distanceDiff = getDistDiff(ds)

    #GPS readings
    gps_bod = gps[0].getValues()

    #Setting colour of robot
    if first_pass == True:
        initial_gps = gps[0].getValues()
        colourrobot, othercolour = set_robot_colour(initial_gps)
        m0,s0,s1,s2,s3,s4,s5,s6,m0s0,m0sta,m0end,r0sta,r0end,r1sta,r1end,r2sta,r2end,r3sta,r3end,r4sta,r4end,r5sta,r5end,r6sta,r6end = searchPath(colourrobot)
        first_pass = False

    #Colour detection
    gvalue = green_light_sensor.getValue()
    rvalue = red_light_sensor.getValue()
    previouscolour = colourresult
    colourresult = get_ls_colour(gvalue, rvalue)

    #Bearings
    previous_bearing = bearing
    rotation = compass[0].getValues()
    bearing = get_bearing()


    #Finding the bearings for one side and the other of a block
    if searching == True and distanceDiff-previousDiff > 50 and f_bearing == None and what_to_do != 1 and what_to_do != 0:
        i_bearing = (bearing + previous_bearing)/2
    if searching == True and distanceDiff-previousDiff < -50  and i_bearing != None and what_to_do != 1 and what_to_do != 0:
        f_bearing = (bearing + previous_bearing)/2
    
    #Finding the bearings for one side and the other of a block, for the two initial search locations
    if searching == True and distanceDiff-previousDiff > 50 and f_bearing == None and (what_to_do == 1 or what_to_do == 0) and (previous_bottom_distance < 900 or bottom_distance < 900):
        i_bearing = (bearing + previous_bearing)/2
    if searching == True and distanceDiff-previousDiff < -50  and i_bearing != None and (what_to_do == 1 or what_to_do == 0) and (previous_bottom_distance < 900 or bottom_distance < 900):
        f_bearing = (bearing + previous_bearing)/2

    #Emergency landing if boxes can't be found and time has run out
    if time > 280:
        goNext = False
        searching = False
        box_dropped = False
        box_dropping = False
        box_in_hold = False
        i_bearing = None
        f_bearing = None
        colourset = None
        approaching = False
        dodged = in_wake(colourrobot)
        if dodged == 1:
            if colourrobot == 'Red':
                resting_place = [0,0,0.4]
            if colourrobot == 'Green':
                resting_place = [0,0,-0.4]
            distance_to = ((gps_bod[0] - resting_place[0])**2 + (gps_bod[2] - resting_place[2])**2)**0.5
            if distance_to < 0.5:
                open_pincers()
            done = goToGPS(resting_place)
            if done == 1:
                quit()

    #Safety against hitting walls
    if (what_to_do == 1 or what_to_do == 0) and top_distance < 350 and previous_top_distance > 350:
        stop()
        goNext = True
        searching = False
        box_dropped = False
        box_dropping = False
        box_in_hold = False
        i_bearing = None
        f_bearing = None
        colourset = None
        approaching = False

    if what_to_do != 1 and what_to_do != 0 and top_distance < 500 and previous_top_distance > 500:
        stop()
        goNext = True
        searching = False
        box_dropped = False
        box_dropping = False
        box_in_hold = False
        i_bearing = None
        f_bearing = None
        colourset = None
        approaching = False

    #Safety against getting stuck
    if what_to_do >= 2 and what_to_do != 4.5 and what_to_do != 7.5:
        if time.is_integer() == True:
            localist[-1] = gps_bod
            for i in range(30):
                localist[i] = localist[i+1]
        if np.round(localist[-1][0], 2) == np.round(localist[-3][0], 2) == np.round(localist[-5][0], 2) == np.round(localist[-7][0], 2) == np.round(localist[-9][0], 2) == np.round(localist[-11][0], 2) == np.round(localist[-13][0], 2) == np.round(localist[-15][0], 2) and np.round(localist[-1][2], 2) == np.round(localist[-3][2], 2) == np.round(localist[-5][2], 2) == np.round(localist[-7][2], 2) == np.round(localist[-9][2], 2) == np.round(localist[-11][2], 2) == np.round(localist[-13][2], 2) == np.round(localist[-15][2], 2):
            stuck = True
            goNext = False
            searching = False
            box_dropped = False
            box_dropping = False
            box_in_hold = False
            i_bearing = None
            f_bearing = None
            colourset = None
            approaching = False
            if previousStuck != stuck:
                stuck_at = gps_bod
                previousStuck = True
            distance_travelled = ((gps_bod[0] - stuck_at[0])**2 + (gps_bod[2] - stuck_at[2])**2)**0.5
            if distance_travelled < 0.2:
                move(-speed_multiplier_b)
                open_pincers()
            else:
                for i in range(30):
                    localist[i] = [0,0,0]
                    localist[-1] = [5,5,5]
                stop()
                goNext = True
                searching = False
                box_dropped = False
                box_dropping = False
                box_in_hold = False
                i_bearing = None
                f_bearing = None
                colourset = None
                approaching = False
                close_pincers()
                stuck = False
                previousStuck = False



    
    #General behaviour of robot
    if searching == False and (box_in_hold == True) and (box_dropping == False) and (box_dropped == False): #What happens when a box of the right colour is enclosed
        box_dropping, dropping_from_here, gps_set,midTrans, what_to_do, previously_left_off = delivering_response(colourset, bearing, gps_bod, gps_red, gps_green, what_to_do, colourrobot,midTrans,box_dropping,dropping_from_here, previously_left_off, box_found_at)
    elif searching == True and box_in_hold == False and box_dropping == False and box_dropped == False: #Search for boxes
        if colourset == 'Green' or colourset == 'Red': #A colour is detected
            box_in_hold, searching, goNext, reverse = followup_colour_detection_response(colourset, colourrobot, othercolour, gps_bod, box_found_at, distance_to_box)
        elif colourset == None and approaching == True: #Approach a box
            goNext, searching, i_bearing, f_bearing, approaching,previously_left_off = approaching_box_response(distanceDiff,previousDiff,goNext, searching, i_bearing, f_bearing, approaching, previously_left_off)
        elif colourset == None: #No colour detected yet
            approaching, goNext, next_mission, searching, previously_left_off, half_measure,close_complete, wtd4_is_done, wtd8_is_done, wtd4_is_done_here, wtd8_is_done_here, distance_to_box, what_to_do = stationary_search_response(f_bearing, i_bearing, bearing, rotEnd, what_to_do, previous_bottom_distance, bottom_distance, colourrobot, half_measure,close_complete,wtd4_is_done, wtd8_is_done, wtd4_is_done_here, wtd8_is_done_here)

    #What to do when a colour is detected
    if (colourresult == 'Green' or colourresult == 'Red') and (previouscolour == None) and searching == True and reverse == False:
        colourset, i_bearing, f_bearing, box_found_at, approaching, previously_left_off = immediate_colour_detection_response(colourresult, previously_left_off, colourrobot)
    
    #What to do when a box is being dropped off
    if box_dropping == True and box_dropped == False:
        box_dropped, box_in_hold, dropped_at_here,boxes_delivered = dropping_box_response(gps_bod, dropping_from_here,what_to_do,boxes_delivered)
    
    #What to do when a colour is dropped
    if box_dropped == True and goNext == False:
        box_dropping, colourset, goNext = after_dropping_box_response(gps_bod, dropped_at_here,what_to_do)
    
    #What to do next
    if goNext == True and searching == False:
        searching, box_dropped, goNext, colourset, what_to_do, next_mission, rotEnd,transition, half_measure,close_complete = go_to_next_search_response(what_to_do, next_mission, half_measure, previously_left_off,transition, colourrobot,close_complete, wtd4_is_done_here, wtd8_is_done_here, time, m0,s0,s1,s2,s3,s4,s5,s6,m0s0,m0sta,m0end,r0sta,r0end,r1sta,r1end,r2sta,r2end,r3sta,r3end,r4sta,r4end,r5sta,r5end,r6sta,r6end)

